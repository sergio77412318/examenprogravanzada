<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>escuela de programadores</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="stylesheet" href="css/style.css">
    
</head>
<body class="baq">
    
<div class="containerform">
        <div class="header">
            <div class="logotitle">
               
                <h2>Escuela de programadores</h2>
            </div>
            <div class="menu">
                <a href="login.php"><li class="">Login</li></a>
                <a href="registro.php"><li class="">Register</li></a>
                <a href="admin.php"><li class="">Administrador</li></a>
            </div>
        </div>
        
        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" class="form">
            <div class=""><h1>Bienvenido</h1></div>
            <div class="">
                <label class=""></label>
                <input type="text" placeholder="Nombre Usuario" name="usuario">
            </div>
            <div class="">
                <label class=""></label>
                <input type="password" placeholder="Contraseña" name="clave">
            </div>
             <?php if(!empty($error)): ?>
            <div class="mensaje">
                <?php echo $error; ?>
            </div>
            <?php endif; ?>
            
            <button type="submit">Entrar<label class=""></label></button>
        </form>
    </div>
    
  
</body>
</html>